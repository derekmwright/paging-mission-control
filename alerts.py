"""
Module for handling Telemetry data from satellites.

Written/Validated for Python 3.7.3

Written by: Derek Wright <derekmwright@gmail.com>
Github: https://github.com/derekmwright
LinkedIn: https://www.linkedin.com/in/derekmwright/

Thanks for providing the coding challenge! I hope you enjoy
reading through my work and find it simple, readable, and extensible.
I look forward to any feedback!

--Derek

"""

import sys
import argparse
import operator
import json
import re
from datetime import datetime, timedelta, timezone

# pylint: disable=line-too-long

# Define rules. Allows for rules to be easily read in and extended """
__RULES__ = [
    {'component': 'BATT', 'count': 3, 'interval': '300', 'expression': 'LESS_THAN', 'limit': 'red_low_limit'},
    {'component': 'TSTAT', 'count': 3, 'interval': '300', 'expression': 'GREATER_THAN', 'limit': 'red_high_limit'},
]

# pylint: enable=line-too-long

class Satellite:
    """ Satellite class for tracking overall components and their health """
    def __init__(self, _id, rules):
        self._id = _id
        self.components = {}
        self.rules = rules

    def __repr__(self):
        return "Satellite({}, {}, {})".format(
            self._id,
            self.components,
            self.rules,
        )

    def update(self, data):
        """ Update the satellite's health with telemetry data. """
        component = data['component']
        if component not in self.components.keys():
            self.__add_component(component)
        self.get_component(component).check(data)

    def __add_component(self, component):
        """ Adds a component to the instance for tracking. Private to protect mutation. """
        self.components[component] = Component(component, self.rules)

    def get_component(self, component):
        """ Get the component based on component name. """
        return self.components[component]

    def alerts(self):
        """ Return a list of alerts across all components """
        # All fun generators and functional tools made this feel a lil too
        # hard to understand/maintain. Not getting clever as a for loop handles this nicely
        alerts = []
        for _, value in self.components.items():
            alerts = alerts + value.alerts
        return alerts

class Component:
    """ Component class for tracking health and any alerts that may be generated. """
    def __init__(self, _type, rules):
        self._type = _type
        self.alerts = []
        # Reject any rules not used for this component
        self.rules = [Rule(**r) for r in rules if r['component'] == self._type]

    def __repr__(self):
        return "Component({}, {}, {})".format(
            self._type,
            self.alerts,
            self.rules,
        )

    def __add_alert(self, alert):
        """ Private method for adding an alert to a component.
            Should only be called via the check() method.
        """
        self.alerts.append(alert)

    def check(self, data):
        """ Takes component telemetry data and executes any rule validations. """
        for rule in self.rules:
            if not rule.validate(data):
                self.__add_alert(
                    # Originally I used Rule.last_violation but noticed the sample data was using
                    # the first time a threshold was triggered and NOT a breach of the rule which
                    # requires <num> violations within a specified time period. This is switched
                    # to use the Rule objects window_start attribute to preserve the original
                    # threshold breach and conform to sample data.
                    Alert(rule.component, rule.window_start, rule.limit)
                )

class Rule:
    """ Instantiates a rule, allowing data to be validated and for violations to be tracked. """
    EXPRESSIONS = {
        'GREATER_THAN': 'gt',
        'LESS_THAN': 'lt',
        'GREATHER_THAN_OR_EQUAL': 'ge',
        'LESS_THAN_OR_EQUAL': 'le',
        'EQUAL': 'eq',
    }

    def __init__(self, component, count, interval, expression, limit):
        # Getting linting errors for this and yes I could make a violation class, etc...
        # however, it felt a overkill at this point and even though a linter yells
        # the code is still easy to follow and maintain... But in a further fleshed out
        # example, a violation class would most likely be necessary.
        self.component = component
        self.count = int(count)
        self.interval = timedelta(seconds=int(interval))
        self.expression = expression
        self.compare = getattr(
            operator,
            self.EXPRESSIONS[self.expression],
        )
        self.limit = limit
        self.last_violation = None
        self.violation_count = 0
        self.window_start = None
        self.window_end = None

    def __repr__(self):
        return "Rule({}, {}, {}, {}, {})".format(
            self.component,
            self.count,
            self.interval,
            self.expression,
            self.limit
        )

    def __reset(self):
        """ Reset counter/time window states. """
        self.last_violation = None
        self.violation_count = 0
        self.window_start = None
        self.window_end = None

    def validate(self, data):
        """ Returns true if data passes the rule or false if data fails the rule.
            If data fails the validation, the violation is tracked based on the rules
            count and violation window (interval).
        """
        timestamp = datetime.strptime(data['timestamp'], '%Y%m%d %H:%M:%S.%f')

        if self.window_end and self.window_end < timestamp:
            # Violation window closed, reset state
            self.__reset()

        if self.compare(float(data['raw_value']), float(data[self.limit])):
            self.__set_violation_window(timestamp)
            if self.__in_violation_window(timestamp):
                self.violation_count += 1
            if self.violation_count >= self.count:
                self.last_violation = timestamp
                return False # Validation fail
            return True
        return True

    # The following two private methods were created to alter
    # functionality of the rules violation window (interval).
    # Currently, the first violation will set the window start (static window).
    # A sliding window can be implement where a future violation will
    # extend the closing of the window and will set the new starting
    # point for the beginning of the window.

    def __set_violation_window(self, timestamp):
        """ Private method to set window start/end """
        if not self.window_start:
            self.window_start = timestamp
            self.window_end = self.window_start + self.interval

    def __in_violation_window(self, timestamp):
        """ Returns true if within window, false if not """
        return self.window_start <= timestamp <= self.window_end

# Making this an object was debatable but extensibility won
class Alert:
    """ Defines an alert object """
    def __init__(self, component, timestamp, limit):
        self.component = component
        self.timestamp = timestamp
        self.severity = limit.lower().replace('limit', '').replace('_', ' ').upper()

    def __repr__(self):
        return "Alert({}, {}, {})".format(
            self.component,
            self.timestamp,
            self.severity,
        )

# App Functions
def parse_data(data):
    """ Parses telemetry data """

    # Fields for mapping delimited string to dict
    fields = [
        'timestamp',
        'satellite_id',
        'red_high_limit',
        'yellow_high_limit',
        'yellow_low_limit',
        'red_low_limit',
        'raw_value',
        'component',
    ]
    for line in data:
        yield dict(zip(fields, line.split('|')))

def main():
    """ Returns json output for alerts """
    parser = argparse.ArgumentParser(
        description='Process telemetry data from a file or via stdin.',
        usage='%(prog)s [-h] [-f FILE] or %(prog)s < FILE',
    )
    parser.add_argument('-f', '--file')
    args = parser.parse_args()

    if args.file:
        file_lines = open(args.file, 'r').read().splitlines()
    else:
        if sys.stdin.isatty():
            parser.print_help()
            sys.exit(1)
        else:
            file_lines = sys.stdin.read().splitlines()

    items = parse_data(file_lines)

    results = []
    satellite_ids = set()
    satellites = {}

    for item in items:
        # Get the saved Satellite object
        satellite_ids.add(item['satellite_id'])
        if item['satellite_id'] not in satellites.keys():
            satellites[item['satellite_id']] = Satellite(item['satellite_id'], __RULES__)
        satellite = satellites[item['satellite_id']]
        satellite.update(item)

    for name, sat in satellites.items():
        for alert in sat.alerts():
            results.append({
                'satelliteId': name,
                'severity': alert.severity,
                'component': alert.component,
                # ugly regex to strip precision off microseconds,
                # might not be needed but not taking chances...
                'timestamp': re.sub(
                    r"000", "", alert.timestamp
                    .replace(tzinfo=timezone.utc)
                    .isoformat()
                    .replace('+00:00', 'Z'))
            })

    print(json.dumps(results))

if __name__ == "__main__":
    main()
